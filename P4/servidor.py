#!/usr/bin/env python

# -*- coding: utf-8 -*-

#Realizado por Jose Carlos Montanez y Jose Crespo

import socket, sys

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

MAX = 65535
PORT = 1060
N = 5

s.bind(('127.0.0.1', PORT))
print 'Listening at', s.getsockname()
#Para controlar el num. de palabras
cont = 0
#Para controlar si nos interesa o no
lista = ['0', '1', '2', '3', '4']
bandera = 0
mensaje = ''
while not bandera:
	data, address = s.recvfrom(MAX)
	if cont < N:
		if data in lista:
			if int(data) == cont:
				data, address = s.recvfrom(MAX)
				mensaje = mensaje + data + " "
				cont = cont + 1
	else:
		print "Mensaje completo. Imprimiendo cadena..."
		print mensaje
		bandera = 1