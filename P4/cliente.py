#!/usr/bin/env python

# -*- coding: utf-8 -*-

import socket, sys

import random

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

MAX = 65535

PORT = 1060

N = 5

mensaje = ["hola", "esto", "es", "un", "mensaje"]

print 'Enviando mensaje...'

s.sendto(str(len(mensaje)), ('127.0.0.1', PORT))

# Intenta enviarlo 100 veces

for i in range(0, 100): 

	aleatorio_i = int(round(random.random()*(N-1)))

	s.sendto(str(aleatorio_i), ('127.0.0.1', PORT)) 

	print "Enviando paquete..."

	s.sendto(mensaje[aleatorio_i], ('127.0.0.1', PORT)) 

	print "Paquete enviado"

    

print 'Mensaje enviado'